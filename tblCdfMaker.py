from scipy.stats import norm
import numpy as np
import os.path
import pickle


class tblCdf:

    def __init__(self, regenerate=False):
        filename = './tblCdf.pickle'
        if not os.path.exists(filename) or regenerate:
            self.tblCdf = np.zeros([10, 10, 10, 10, 10, 10])
            for i in range(10):
                for j in range(10):
                    for k in range(10):
                        for l in range(10):
                            for m in range(10):
                                for n in range(10):
                                    val = int(i)
                                    val += int(j) / 10
                                    val += int(k) / 100
                                    val += int(l) / 1000
                                    val += int(m) / 10000
                                    val += int(n) / 100000
                                    # print(val)
                                    self.tblCdf[i][j][k][l][m][n] = norm.cdf(val)
            with open(filename, 'wb') as f:
                pickle.dump(self.tblCdf, f)
            return None
        else:
            with open(filename, 'rb') as f:
                self.tblCdf = pickle.load(f)

    def cdf(self, argVal):
        val = argVal
        minusFlag = False
        if val < 0:
            val = -val
            minusFlag = True
        if val > 10:
            val = 9.99999
        a = int(val) % 10
        b = int(val * 10) % 10
        c = int(val * 100) % 10
        d = int(val * 1000) % 10
        e = int(val * 10000) % 10
        f = int(val * 100000) % 10
        if minusFlag:
            return 1 - self.tblCdf[a][b][c][d][e][f]
        else:
            return self.tblCdf[a][b][c][d][e][f]
