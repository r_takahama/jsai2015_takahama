from numpy.random import *
from KL import KL
from glicko import glicko
from winrate import winrate
import numpy as np
import math
from scipy.stats import spearmanr
from scipy.stats import rankdata

debug = False


class sampling:

    def __init__(self, numObj, lstMu, lstSqSigma, comparisonStyle='Pairwise', samplingStyle='Random', utilStyle='CoM_OSL'):
        if samplingStyle is 'Active':
            print('initializing tblActiveUtility...')
            self.wr = winrate()
            self.gl = glicko()
            self.kl = KL()
            self.tblActiveUtility = np.zeros((numObj, numObj))
            self.tblWinRate = np.zeros((numObj, numObj))

            initValWinRate = self.wr.winrate(lstMu[0], lstMu[1], lstSqSigma[0], lstSqSigma[1])
            for i in range(numObj):
                for j in range(i + 1, numObj):
                    # self.tblActiveUtility[i][j] = self.calcUtilityCWR(numObj, lstMu, lstSqSigma, i, j)
                    # self.tblWinRate[i][j] = initValWinRate
                    self.tblWinRate[i][j] = self.wr.winrate(lstMu[i], lstMu[j], lstSqSigma[i], lstSqSigma[j])
            # print(self.tblWinRate)

            initValActiveUtility = self.calcUtility(numObj, lstMu, lstSqSigma, 0, 1, utilStyle)
            for i in range(numObj):
                for j in range(i + 1, numObj):
                    # self.tblActiveUtility[i][j] = self.calcUtilityCWR(numObj, lstMu, lstSqSigma, i, j)
                    # self.tblActiveUtility[i][j] = initValActiveUtility
                    self.tblActiveUtility[i][j] = self.calcUtility(numObj, lstMu, lstSqSigma, i, j, utilStyle)
            if debug is True:
                print(self.tblActiveUtility)
        return None

    def sampling(self, numObj, lstMu, lstSqSigma, comparisonStyle='Pairwise', samplingStyle='Random', before=None):
        if comparisonStyle is 'Pairwise':
            if samplingStyle is 'Random':
                return self.PairwiseRandom(numObj, lstMu, lstSqSigma)
            elif samplingStyle is 'Active':
                return self.PairwiseActive(numObj, lstMu, lstSqSigma)
        elif comparisonStyle == 'Progressive':
            if samplingStyle is 'Random':
                return self.ProgressiveRandom(numObj, lstMu, lstSqSigma, before)
            elif samplingStyle is 'Active':
                return self.ProgressiveActive(numObj, lstMu, lstSqSigma, before)

    def PairwiseRandom(self, numObj, lstMu, lstSqSigma):
        self.i = randint(numObj)
        self.j = randint(numObj)
        while self.i == self.j:
            self.j = randint(numObj)
        return self.i, self.j, None

    def PairwiseActive(self, numObj, lstMu, lstSqSigma):
        index = np.argmax(self.tblActiveUtility)
        # print(index)
        x = index // numObj
        y = index % numObj
        # print(x, y, self.tblActiveUtility[x][y])
        # print(self.tblActiveUtility)
        return x, y, None

    def ProgressiveRandom(self, numObj, lstMu, lstSqSigma, before=None):
        if before is None:
            tpl = self.PairwiseRandom(numObj, lstMu, lstSqSigma)
            return tpl[0], tpl[1], tpl[1]
        else:
            new = randint(numObj)
            while new == before:
                new = randint(numObj)
        return before, new, new

    def ProgressiveActive(self, numObj, lstMu, lstSqSigma, before=None):
        if before is None:
            tpl = self.PairwiseActive(numObj, lstMu, lstSqSigma)
            if debug is True:
                print(tpl[0], tpl[1], tpl[1])
            return tpl[0], tpl[1], tpl[1]
        else:
            max_u = 0
            xOfMax_u = 0
            yOfMax_u = 1
            before_next = 0
            for i in range(before):
                val = self.tblActiveUtility[i][before]
                if val > max_u:
                    max_u = val
                    xOfMax_u = i
                    yOfMax_u = before
                    before_next = i
            for j in range(before + 1, numObj):
                val = self.tblActiveUtility[before][j]
                if val > max_u:
                    max_u = val
                    xOfMax_u = before
                    yOfMax_u = j
                    before_next = j
            if debug is True:
                print(xOfMax_u, yOfMax_u, before_next, max_u)
            return xOfMax_u, yOfMax_u, before_next

    def reCalcTblActiveUtility(self, numObj, lstMu, lstSqSigma, x, y, utilStyle='CoM_OSL', usingIndex=None):
        bufTblWinRate = np.zeros((numObj, numObj))
        for i in range(numObj):
            for j in range(i + 1, numObj):
                bufTblWinRate[i][j] = self.wr.winrate(lstMu[i], lstMu[j], lstSqSigma[i], lstSqSigma[j])
        self.tblWinRate = np.array(list(bufTblWinRate.tolist()))
        # print(self.tblWinRate)

        if utilStyle is 'CoM_TSL':
            for i in range(usingIndex):
                u = self.calcUtility(numObj, lstMu, lstSqSigma, i, usingIndex, utilStyle, usingIndex)
                self.tblActiveUtility[i][usingIndex] = u
            for j in range(usingIndex + 1, numObj):
                u = self.calcUtility(numObj, lstMu, lstSqSigma, usingIndex, j, utilStyle, usingIndex)
                self.tblActiveUtility[usingIndex][j] = u
        else:
            for i in range(y):
                u = self.calcUtility(numObj, lstMu, lstSqSigma, i, y, utilStyle, usingIndex)
                self.tblActiveUtility[i][y] = u
            for j in range(x + 1, numObj):
                u = self.calcUtility(numObj, lstMu, lstSqSigma, x, j, utilStyle, usingIndex)
                self.tblActiveUtility[x][j] = u
            for i in range(x):
                u = self.calcUtility(numObj, lstMu, lstSqSigma, i, x, utilStyle, usingIndex)
                self.tblActiveUtility[i][x] = u
            for j in range(y + 1, numObj):
                u = self.calcUtility(numObj, lstMu, lstSqSigma, y, j, utilStyle, usingIndex)
                self.tblActiveUtility[y][j] = u
        # for i in range(numObj):
        #     for j in range(i + 1, numObj):
        #         self.tblActiveUtility[i][j] = self.calcUtility(numObj, lstMu, lstSqSigma, i, j, utilStyle)
        if debug is True:
            print(self.tblActiveUtility)

    def calcUtility(self, numObj, lstMu, lstSqSigma, x, y, utilStyle='CoM_OSL', usingIndex=None, alpha=0.5):
        if utilStyle is 'CoM_OSL':
            return self.calcUtilityCoM_OSL(lstMu, lstSqSigma, x, y)
        elif utilStyle is 'CoM_TSL':
            # print('calcUtility', x, y, usingIndex)
            if usingIndex == x:
                usingIndex = y
            elif usingIndex == y:
                usingIndex = x
            else:
                usingIndex = None
            return self.calcUtilityCoM_TSL(numObj, lstMu, lstSqSigma, x, y, usingIndex, alpha)
        elif utilStyle is 'CWR_OSL_KLD':
            return self.calcUtilityCWR_OSL(numObj, lstMu, lstSqSigma, x, y, 'KLD')
        elif utilStyle is 'CWR_OSL_SAE':
            return self.calcUtilityCWR_OSL(numObj, lstMu, lstSqSigma, x, y, 'SAE')
        elif utilStyle is 'CoR_OSL':
            return self.calcUtilityCoR_OSL(numObj, lstMu, lstSqSigma, x, y)

    def calcUtilityCoM_OSL(self, lstMu, lstSqSigma, x, y):
        # print('CoM_OSL')
        sqSigma_x = self.gl.updatedSquareOfSigma_i(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        sqSigma_y = self.gl.updatedSquareOfSigma_i(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        Mu_x_xwin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 1)
        Mu_y_xwin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 0)
        Mu_x_ywin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 0)
        Mu_y_ywin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 1)
        fst = self.wr.winrate(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        snd = self.kl.KLdivergence(lstMu[x], Mu_x_xwin, lstSqSigma[x], sqSigma_x)
        trd = self.kl.KLdivergence(lstMu[y], Mu_y_xwin, lstSqSigma[y], sqSigma_y)
        fth = self.wr.winrate(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        fvth = self.kl.KLdivergence(lstMu[x], Mu_x_ywin, lstSqSigma[x], sqSigma_x)
        sth = self.kl.KLdivergence(lstMu[y], Mu_y_ywin, lstSqSigma[y], sqSigma_y)
        u = fst * (snd + trd) + fth * (fvth + sth)
        return u

    def calcUtilityCoM_TSL(self, numObj, lstMu, lstSqSigma, x, y, usingIndex, alpha):
        # print('CoM_TSL')
        if usingIndex is None:
            # print('usingIndex is None. return float("inf")')
            return float("inf")

        # calculate updated parameters
        sqSigma_x = self.gl.updatedSquareOfSigma_i(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        sqSigma_y = self.gl.updatedSquareOfSigma_i(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        Mu_x_xwin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 1)
        Mu_y_xwin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 0)
        Mu_x_ywin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 0)
        Mu_y_ywin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 1)

        # calculate g1
        probXwin = self.wr.winrate(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        KLDX_xwin = self.kl.KLdivergence(lstMu[x], Mu_x_xwin, lstSqSigma[x], sqSigma_x)
        KLDY_xwin = self.kl.KLdivergence(lstMu[y], Mu_y_xwin, lstSqSigma[y], sqSigma_y)
        probYwin = self.wr.winrate(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        KLDX_ywin = self.kl.KLdivergence(lstMu[x], Mu_x_ywin, lstSqSigma[x], sqSigma_x)
        KLDY_ywin = self.kl.KLdivergence(lstMu[y], Mu_y_ywin, lstSqSigma[y], sqSigma_y)
        g1 = probXwin * (KLDX_xwin + KLDY_xwin) + probYwin * (KLDX_ywin + KLDY_ywin)

        # calculate g2
        lstSqSigma_new = list(lstSqSigma)
        lstSqSigma_new[x] = sqSigma_x
        lstSqSigma_new[y] = sqSigma_y
        # utility of two step ahead when x wins
        lstMu_xwin = list(lstMu)
        lstMu_xwin[x] = Mu_x_xwin
        lstMu_xwin[y] = Mu_y_xwin
        utilTSA_xwin = self.estimateUtiilityOfTwoStepAhead(numObj, lstMu_xwin, lstSqSigma_new, usingIndex)
        # utility of two step ahead when y wins
        lstMu_ywin = list(lstMu)
        lstMu_ywin[x] = Mu_x_ywin
        lstMu_ywin[y] = Mu_y_ywin
        utilTSA_ywin = self.estimateUtiilityOfTwoStepAhead(numObj, lstMu_ywin, lstSqSigma_new, usingIndex)
        g2 = probXwin * utilTSA_xwin + probYwin * utilTSA_ywin

        u = g1 + alpha * g2
        return u

    def estimateUtiilityOfTwoStepAhead(self, numObj, lstMu, lstSqSigma, usingIndex):
        max_u = 0
        # print('estimateUtiilityOfTwoStepAhead')
        for i in range(usingIndex):
            # print(i, usingIndex)
            u = self.calcUtilityCoM_OSL(lstMu, lstSqSigma, i, usingIndex)
            if u > max_u:
                max_u = u
        for j in range(usingIndex + 1, numObj):
            # print(usingIndex, j)
            u = self.calcUtilityCoM_OSL(lstMu, lstSqSigma, usingIndex, j)
            if u > max_u:
                max_u = u
        return max_u

    def calcUtilityCWR_OSL(self, numObj, lstMu, lstSqSigma, x, y, winRateDistStyle):
        # print('CWR_OSL')
        sqSigma_x = self.gl.updatedSquareOfSigma_i(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        sqSigma_y = self.gl.updatedSquareOfSigma_i(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        Mu_x_xwin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 1)
        Mu_y_xwin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 0)
        Mu_x_ywin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 0)
        Mu_y_ywin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 1)
        lstSqSigma_new = list(lstSqSigma)
        lstSqSigma_new[x] = sqSigma_x
        lstSqSigma_new[y] = sqSigma_y
        # calc change of winning rate when x wins
        lstMu_xwin = list(lstMu)
        lstMu_xwin[x] = Mu_x_xwin
        lstMu_xwin[y] = Mu_y_xwin
        sumCWR = 0
        for i in range(y):
            fst = self.wr.winrate(lstMu_xwin[i], lstMu_xwin[y], lstSqSigma_new[i], lstSqSigma_new[y])
            snd = self.tblWinRate[i][y]
            sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        for j in range(x + 1, numObj):
            if j != y:
                fst = self.wr.winrate(lstMu_xwin[x], lstMu_xwin[j], lstSqSigma_new[x], lstSqSigma_new[j])
                snd = self.tblWinRate[x][j]
                sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        for i in range(x):
            fst = self.wr.winrate(lstMu_xwin[i], lstMu_xwin[x], lstSqSigma_new[i], lstSqSigma_new[x])
            snd = self.tblWinRate[i][x]
            sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        for j in range(y + 1, numObj):
            fst = self.wr.winrate(lstMu_xwin[y], lstMu_xwin[j], lstSqSigma_new[y], lstSqSigma_new[j])
            snd = self.tblWinRate[y][j]
            sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        sumCWR_xwin = sumCWR
        # calc change of winning rate when i wins
        lstMu_ywin = list(lstMu)
        lstMu_ywin[x] = Mu_x_ywin
        lstMu_ywin[y] = Mu_y_ywin
        sumCWR = 0
        for i in range(y):
            fst = self.wr.winrate(lstMu_ywin[i], lstMu_ywin[y], lstSqSigma_new[i], lstSqSigma_new[y])
            snd = self.tblWinRate[i][y]
            sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        for j in range(x + 1, numObj):
            if j != y:
                fst = self.wr.winrate(lstMu_ywin[x], lstMu_ywin[j], lstSqSigma_new[x], lstSqSigma_new[j])
                snd = self.tblWinRate[x][j]
                sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        for i in range(x):
            fst = self.wr.winrate(lstMu_ywin[i], lstMu_ywin[x], lstSqSigma_new[i], lstSqSigma_new[x])
            snd = self.tblWinRate[i][x]
            sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        for j in range(y + 1, numObj):
            fst = self.wr.winrate(lstMu_ywin[y], lstMu_ywin[j], lstSqSigma_new[y], lstSqSigma_new[j])
            snd = self.tblWinRate[y][j]
            sumCWR += self.getDistanceOfTwoProbabilities(fst, snd, winRateDistStyle)
        sumCWR_ywin = sumCWR
        fst = self.wr.winrate(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        snd = sumCWR_xwin
        trd = self.wr.winrate(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        fth = sumCWR_ywin
        # print(fst * snd + trd * fth)
        return fst * snd + trd * fth

    def getDistanceOfTwoProbabilities(self, fst, snd, winRateDistStyle):
        if winRateDistStyle is 'KLD':
            return self.kl.KLDivergenceBernoulli(fst, snd)
        elif winRateDistStyle is 'SAE':
            return math.fabs(fst - snd)

    def calcUtilityCoR_OSL(self, numObj, lstMu, lstSqSigma, x, y):
        # if debug is True:
        #     print('CoR_OSL')
        sqSigma_x = self.gl.updatedSquareOfSigma_i(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        sqSigma_y = self.gl.updatedSquareOfSigma_i(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        Mu_x_xwin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 1)
        Mu_y_xwin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 0)
        Mu_x_ywin = self.gl.updatedNu_i(lstMu[x], lstMu[y], sqSigma_x, lstSqSigma[y], 0)
        Mu_y_ywin = self.gl.updatedNu_i(lstMu[y], lstMu[x], sqSigma_y, lstSqSigma[x], 1)
        probXwin = self.wr.winrate(lstMu[x], lstMu[y], lstSqSigma[x], lstSqSigma[y])
        probYwin = self.wr.winrate(lstMu[y], lstMu[x], lstSqSigma[y], lstSqSigma[x])
        # calculate spearman's rho in case of x wins
        lstMu_xwin = list(lstMu)
        lstMu_xwin[x] = Mu_x_xwin
        lstMu_xwin[y] = Mu_y_xwin
        # spearman_xwin = spearmanr(lstMu_xwin, lstMu)[0]
        dSpear_xwin = np.linalg.norm(rankdata(lstMu_xwin) - rankdata(lstMu))
        # calculate spearman's rho in case of y wins
        lstMu_ywin = list(lstMu)
        lstMu_ywin[x] = Mu_x_ywin
        lstMu_ywin[y] = Mu_y_ywin
        # spearman_ywin = spearmanr(lstMu_ywin, lstMu)[0]
        dSpear_ywin = np.linalg.norm(rankdata(lstMu_ywin) - rankdata(lstMu))
        # print(lstMu, lstMu_xwin, lstMu_ywin, dSpear_xwin, dSpear_ywin)
        # return expectation of spearman's rho
        # return - (probXwin * spearman_xwin + probYwin * spearman_ywin)
        return probXwin * dSpear_xwin + probYwin * dSpear_ywin


def test():
    s = sampling(100, [x for x in range(100)], [100 for x in range(100)], 'Progressive', 'Active', 'CWR')
    s.reCalcTblActiveUtility(100, [x for x in range(100)], [100 for x in range(100)], 10, 20, 'CWR')

if __name__ == '__main__':
    s = sampling(100, [x for x in range(100)], [100 for x in range(100)], 'Progressive', 'Active')
    s.reCalcTblActiveUtility(100, [x for x in range(100)], [100 for x in range(100)], 10, 20, 'KLD')
