import numpy as np
from numpy.random import *
from glicko import glicko
from sampling import sampling
import pickle
import os
from scipy.stats import rankdata

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--datatype')
parser.add_argument('--start', action='store_true', default=False)
args = parser.parse_args()

lstItems = [
    {'item_id': 0, 'item_name': 'whitewhite0'},
    {'item_id': 1, 'item_name': 'whitewhite1'},
    {'item_id': 2, 'item_name': 'whitewhite2'},
    {'item_id': 3, 'item_name': 'whitewhite3'},
    {'item_id': 4, 'item_name': 'whitewhite4'}
]
numObj = len(lstItems)

# load data from pickle file
filename = 'test.pickle'
if os.path.exists(filename) and args.start is False:
    with open(filename, 'rb') as f:
        lstFromPickle = pickle.load(f)
    dispItems = lstFromPickle[-1]['displayed_items']
    respItem = lstFromPickle[-1]['response_item']
    if respItem == '':
        print('Error: "response_item" of the newest trial is not stored.')
        exit(0)
else:
    lstFromPickle = []
# print('lstFromPickle:', lstFromPickle)
numTrial = len(lstFromPickle)

# initialize ESTIMATED score(mean) and variance of objects
if numTrial == 0:
    estParams = {}
    for i in range(numObj):
        estParams[i] = {'mu': 1500, 'sigma': 147 ** 2}
else:
    # set the result of the newest trial as estPatams
    estParams = lstFromPickle[-1]['estimated_parameters']

    # update parameters(mean and variance) with Glicko Update Equations
    g = glicko()
    i = dispItems[0]
    j = dispItems[1]
    if i == respItem:
        s_i = 1
        s_j = 0
    else:
        s_i = 0
        s_j = 1
    mu_i = estParams[i]['mu']
    mu_j = estParams[j]['mu']
    sigma_i = estParams[i]['sigma']
    sigma_j = estParams[j]['sigma']
    updatedSqSigma_i = g.updatedSquareOfSigma_i(mu_i, mu_j, sigma_i, sigma_j)
    updatedSqSigma_j = g.updatedSquareOfSigma_i(mu_j, mu_i, sigma_j, sigma_i)
    updatedMu_i = g.updatedNu_i(mu_i, mu_j, updatedSqSigma_i, sigma_j, s_i)
    updatedMu_j = g.updatedNu_i(mu_j, mu_i, updatedSqSigma_j, sigma_i, s_j)
    estParams[i]['sigma'] = updatedSqSigma_i
    estParams[j]['sigma'] = updatedSqSigma_j
    estParams[i]['mu'] = updatedMu_i
    estParams[j]['mu'] = updatedMu_j

# determine the next evaluated objects
before_next = None
lstMu = [estParams[x]['mu'] for x in range(numObj)]
lstSqSigma = [estParams[x]['sigma'] for x in range(numObj)]
comparisonStyle = 'Pairwise'
samplingStyle = 'Active'
utilStyle = 'CWR_OSL_KLD'
s = sampling(numObj, lstMu, lstSqSigma, comparisonStyle, samplingStyle, utilStyle)
i, j, before_next = s.sampling(numObj, lstMu, lstSqSigma, comparisonStyle, samplingStyle, before=before_next)

# store result into list
lstResult = lstFromPickle
lstResult.append({
    'trial_id': numTrial,
    'displayed_items': (i, j),
    'response_timestamp': '',
    # 'response_item': np.random.choice((i, j)),
    'response_item': '',
    'estimated_parameters': estParams,
    'estimated_ranking': rankdata(lstMu, method='min')
})

# dump lstResult
with open(filename, 'wb') as f:
    pickle.dump(lstResult, f)

# print('lstResult[-1]:', lstResult[-1])
