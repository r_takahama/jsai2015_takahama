import math


class glicko:

    def __init__(self):
        self.q = math.log(10) / 400

    def g(self, squareOfSigma):
        return 1 / math.sqrt(1 + 3 * self.q ** 2 * squareOfSigma / math.pi ** 2)

    def E(self, nu_i, nu_j, squareOfSigma_j):
        return 1 / (1 + 10 ** (- self.g(squareOfSigma_j) * (nu_i - nu_j) / 400))

    def squareOfDelta(self, nu_i, nu_j, squareOfSigma_j):
        firstTerm = 1 / (self.q ** 2 * self.g(squareOfSigma_j) ** 2)
        e = self.E(nu_i, nu_j, squareOfSigma_j)
        secondTerm = 1 / (e * (1 - e))
        return firstTerm * secondTerm

    def updatedSquareOfSigma_i(self, nu_i, nu_j, squareOfSigma_i, squareOfSigma_j):
        firstTerm = 1 / squareOfSigma_i
        secondTerm = 1 / self.squareOfDelta(nu_i, nu_j, squareOfSigma_j)
        return 1 / (firstTerm + secondTerm)

    def updatedNu_i(self, nu_i, nu_j, updatedSquareOfSigma_i, squareOfSigma_j, s_i):
        firstTerm = self.q * updatedSquareOfSigma_i
        secondTerm = self.g(squareOfSigma_j)
        thirdTerm = s_i - self.E(nu_i, nu_j, squareOfSigma_j)
        return nu_i + firstTerm * secondTerm * thirdTerm

if __name__ == "__main__":

    nu_a = 1700
    nu_b = 1500
    squareOfSigma_a = 21609
    squareOfSigma_b = 21609

    glicko = glicko()

    updatedSquareOfSigma_a = glicko.updatedSquareOfSigma_i(nu_a, nu_b, squareOfSigma_a, squareOfSigma_b)
    updatedSquareOfSigma_b = glicko.updatedSquareOfSigma_i(nu_b, nu_a, squareOfSigma_b, squareOfSigma_a)
    updatedNu_a = glicko.updatedNu_i(nu_a, nu_b, updatedSquareOfSigma_a, squareOfSigma_b, 1)
    updatedNu_b = glicko.updatedNu_i(nu_b, nu_a, updatedSquareOfSigma_b, squareOfSigma_a, 0)

    print(updatedNu_a, updatedNu_b, updatedSquareOfSigma_a, updatedSquareOfSigma_b)
